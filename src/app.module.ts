import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {MessageEntity} from "./entity/message.entity";
import {ConfigModule} from "@nestjs/config";

@Module({
  imports: [ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
    type: 'mysql',
    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PWD,
    database: process.env.DATABASE_NAME,
    entities: [MessageEntity],
    synchronize: true,
  }),TypeOrmModule.forFeature([MessageEntity])],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
